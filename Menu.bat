@echo off
MODE CON COLS=40 LINES=18
goto menu

:menu
cls
echo ------------------------------
echo -         1. START           -
echo -                            -
echo -         2. INFO            -
echo -                            -
echo -         3. BACKUP          -
echo -                            -   
echo -         4. EXIT            -   
echo ------------------------------

Setlocal
SET /P key=Choose an key: 
if "%key%"=="1" call :start
if "%key%"=="2" call :info
if "%key%"=="3" call :backup
if "%key%"=="4" EXIT
Endlocal
pause
goto menu

:start
  cls
  start Fibonacci.jar
  timeout /t 2
  cd raport
  python Raport.py %*
  cd ..
  GOTO :EOF

:backup
  Setlocal
  for /F "tokens=1-4 delims=/ " %%A in ('date/t') do (
    set DateDay=%%A
    set DateMonth=%%B
    set DateYear=%%C
  )
  set CurrentDate=%DateDay%-%DateMonth%-%DateYear%-%time:~0,2%.%time:~3,2%
  md "backup\%CurrentDate%"
  md "backup\%CurrentDate%\input"
  md "backup\%CurrentDate%\raport"
  copy *.txt "backup\%CurrentDate%\input"
  copy raport "backup\%CurrentDate%\raport"
  Endlocal
  GOTO :EOF

:info
  cls
  echo -----------------------------
  echo Jezyki Skryptowe
  echo Autor: Aleksander Fedyczek
  echo Informatyka semestr 3. gr 1A
  echo -----------------------------
  GOTO :EOF

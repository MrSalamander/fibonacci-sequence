import os
import datetime as dt


# wczytanie pliku output.txt do tablicy [0][...]
file = open("..\input.txt", "r").read()
input = file.split("\n")

# wczytanie pliku output.txt do tablicy [1][...]
file = open("..\output.txt", "r").read()
output = file.split("\n")


fileOut = open(f"raport.html", "w") #plik z wynikiem
fileOut.write(f"""<!doctype html>
<title>Example</title>
<head>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
 <header> ------------------------------------ <br/>
   Jezyki Skryptowe<br/>
   Autor: Aleksander Fedyczek<br/>
   Informatyka semestr 3. gr 1A<br/>
   ------------------------------------</header>
  <div>
  <table id="left">
  <tr><td colspan='2'>plik input.txt</td></tr>
  <tr><td>Input</td><td>Znaczenie</td></tr>
  <tr><td>{input[0]}</td><td>Od którego wyrazu zaczynam</td></tr>
  <tr><td>{input[1]}</td><td>Ile wyrazów wyznaczyć</td></tr>
  <tr><td>{input[2]}</td><td>Co który wyraz</td></tr>
  </table>
  </div>

  <div class="col-1">
    <main class="content">
    <table>\n""")
fileOut.write("""<tr>\n<td colspan='2'>Wybrane wyrazy ciągu Fibonacciego</td>\n
    </tr>""")
fileOut.write("""<tr>\n<td>Nr. wyrazu</td><td>Wartość</td>\n</tr>""")
for line in output:
    words = line.split()
    fileOut.write("""<tr>\n""")
    for word in words:
        fileOut.write(f"""<td>{word}</td>""")
    fileOut.write("""</tr>\n""")
fileOut.write("""

    </main>
  </div>

</body>""")
fileOut.close()




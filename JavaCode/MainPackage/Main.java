package MainPackage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // read from file "input.txt" 3 valuses
        // x - tab[0] - start position
        // y - tab[1] - amount of generated numbers
        // z - tab[2] - iteration jump
        Scanner inputer = new Scanner(System.in);
        int[] tab = new int[3];
        readFromFile(tab);

        // write result to file "output.txt"
        // format:  Position (n) GeneratedNumber (Fn)
        try {
            FileWriter fw = new FileWriter("output.txt");
            int n = tab[0];
            long startTime = System.nanoTime(); // get start time
            for(int i=0;i<tab[1];i++)
            {
                BigDecimal pier = BigDecimal.ONE.valueOf(5).sqrt(MathContext.DECIMAL64); // pierwiastek z 5
                BigDecimal pierPiec = BigDecimal.ONE.divide(pier, 2, RoundingMode.UP); // 1/pier(5)
                BigDecimal part1 = ((BigDecimal.ONE.add(pier)).divide(BigDecimal.valueOf(2),2, RoundingMode.UP)).pow(n);
                BigDecimal part2 = ((BigDecimal.ONE.subtract(pier)).divide(BigDecimal.valueOf(2),2, RoundingMode.UP)).pow(n);
                BigInteger Fn = (pierPiec.multiply(part1).subtract(pierPiec.multiply(part2))).toBigInteger();

                if(n==2) Fn = BigInteger.ONE;
                String temp = String.format("%d     %d\r\n",n,Fn);
                fw.write(temp);
                n+=tab[2];
            }
            fw.close();
            long endTime = System.nanoTime(); // get end time

            long duration = (endTime - startTime)/1000000; //return miliseconds
            System.out.printf("the operation lasted : %d miliseconds", duration);
        }catch(IOException e){
            System.out.println(e.getLocalizedMessage());
        }
    }


    protected static void readFromFile(int[] tab){
        BufferedReader reader;
        try{
            int i = 0;
            reader = new BufferedReader(new FileReader("input.txt"));
            String line = reader.readLine();
            while(line != null){
                System.out.println(line);
                tab[i] = Integer.parseInt(line);
                i++;
                line = reader.readLine();
            }
            reader.close();
        }catch(IOException e){
            e.printStackTrace();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
}
